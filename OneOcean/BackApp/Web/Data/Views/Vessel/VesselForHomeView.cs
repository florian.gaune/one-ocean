﻿namespace OneOcean.BackApp.Web.Data.Views.Vessel;

public class VesselForHomeView
{
    public string Id { get; set; }
    public string Name { get; set; }
    public double AverageSpeed { get; set; }
    public double TotalDistance { get; set; }
}