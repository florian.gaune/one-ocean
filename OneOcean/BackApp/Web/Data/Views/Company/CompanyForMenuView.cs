﻿namespace OneOcean.BackApp.Web.Data.Views;

public class CompanyForMenuView
{
    public string Id { get; set; }
    public string Name { get; set; }
}