﻿using OneOcean.BackApp.Web.Data.Views.Vessel;

namespace OneOcean.BackApp.Web.Data.Views;

public class CompanyForHomeView : CompanyForMenuView
{
    public List<string> VesselIdArray { get; set; }
}