﻿using Microsoft.AspNetCore.Mvc;
using OneOcean.BackApp.Services.Contracts;
using OneOcean.BackApp.Web.Data.Views.Vessel;

namespace OneOcean.BackApp.Web.Controllers;

[ApiController]
[Route("vessel")]
public class VesselController
{
    private readonly IVesselService VesselService;

    public VesselController(IVesselService vesselService)
    {
        this.VesselService = vesselService;
    }

    [HttpGet]
    [Route("{id:guid}")]
    public VesselForHomeView FindVesselForHomeView(Guid id)
    {
        return this.VesselService.FindVesselForHomeViewById(id);
    }
}