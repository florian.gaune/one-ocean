﻿using Microsoft.AspNetCore.Mvc;
using OneOcean.BackApp.Services.Contracts;
using OneOcean.BackApp.Web.Data.Views;

namespace OneOcean.BackApp.Web.Controllers;

[ApiController]
[Route("company")]
public class CompanyController : ControllerBase
{
    private readonly ICompanyService CompanyService;

    public CompanyController(ICompanyService companyService)
    {
        this.CompanyService = companyService;
    }


    [HttpGet]
    [Route("{id:guid}")]
    public CompanyForMenuView FindCompanyForMenuView(Guid id)
    {
        return this.CompanyService.FindCompanyForHomeViewById(id);
    }
}