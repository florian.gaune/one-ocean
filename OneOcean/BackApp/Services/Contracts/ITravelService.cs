﻿using OneOcean.BackApp.Data.Domain;
using OneOcean.BackApp.Web.Data.Views.Vessel;

namespace OneOcean.BackApp.Services.Contracts;

public interface ITravelService
{
    /// <summary>
    /// Initialise data on vessel for home view format.
    /// In this version, it will :
    ///     - calculate total distance
    ///     - calculate average speed
    /// </summary>
    /// <param name="vesselView">the vessel in homeView format to initialise</param>
    void InitDataFromTravelInVesselForHomeView(VesselForHomeView vesselView);
}