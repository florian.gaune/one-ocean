﻿using OneOcean.BackApp.Web.Data.Views.Vessel;

namespace OneOcean.BackApp.Services.Contracts;

public interface IVesselService
{
    /// <summary>
    /// Find the related vessel in homeView format
    /// </summary>
    /// <param name="id">id of the searched vessel</param>
    /// <returns>the vessel on homeView format</returns>
    public VesselForHomeView FindVesselForHomeViewById(Guid id);
}