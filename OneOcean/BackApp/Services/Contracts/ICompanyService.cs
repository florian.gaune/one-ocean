﻿using OneOcean.BackApp.Web.Data.Views;

namespace OneOcean.BackApp.Services.Contracts;

public interface ICompanyService
{
    /// <summary>
    /// Find company data related to the given id.
    /// The company is for the homeView
    /// </summary>
    /// <param name="id">id of the searched company</param>
    /// <returns>the company in homeView format</returns>
    CompanyForHomeView FindCompanyForHomeViewById(Guid id);
}