﻿using OneOcean.BackApp.Data.Domain;
using OneOcean.BackApp.Data.Repository.Contracts;
using OneOcean.BackApp.Services.Contracts;
using OneOcean.BackApp.Web.Data.Views;

namespace OneOcean.BackApp.Services.Implementation;

public class CompanyService : ICompanyService
{
    private readonly ICompanyRepository CompanyRepository;

    public CompanyService(ICompanyRepository companyRepository)
    {
        this.CompanyRepository = companyRepository;
    }

    public CompanyForHomeView FindCompanyForHomeViewById(Guid id)
    {
        Company? company = this.CompanyRepository.FindCompanyById(id);

        if (company == null)
        {
            throw new Exception();
        }

        return new CompanyForHomeView()
        {
            Id = company.Id.ToString(),
            Name = company.Name,
            VesselIdArray = company.Vessels.Select(vessel => vessel.Id.ToString()).ToList()
        };
    }
}