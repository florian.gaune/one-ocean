﻿using OneOcean.BackApp.Data.Domain;
using OneOcean.BackApp.Data.Repository.Contracts;
using OneOcean.BackApp.Services.Contracts;
using OneOcean.BackApp.Web.Data.Views.Vessel;

namespace OneOcean.BackApp.Services.Implementation;

public class VesselService : IVesselService
{
    private readonly IVesselRepository VesselRepository;
    private readonly ITravelService TravelService;

    public VesselService(IVesselRepository vesselRepository, ITravelService travelService)
    {
        this.VesselRepository = vesselRepository;
        this.TravelService = travelService;
    }

    public VesselForHomeView FindVesselForHomeViewById(Guid id)
    {
        Vessel? vessel = this.VesselRepository.FindVesselById(id);

        if (vessel == null)
        {
            throw new Exception();
        }

        VesselForHomeView vesselToReturn = new VesselForHomeView
        {
            Id = vessel.Id.ToString(),
            Name = vessel.Name
        };
        
        this.TravelService.InitDataFromTravelInVesselForHomeView(vesselToReturn);
        return vesselToReturn;
    }
}