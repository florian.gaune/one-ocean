﻿using OneOcean.BackApp.Data.Domain;
using OneOcean.BackApp.Data.Repository.Contracts;
using OneOcean.BackApp.Services.Contracts;
using OneOcean.BackApp.Web.Data.Views.Vessel;

namespace OneOcean.BackApp.Services.Implementation;

public class TravelService : ITravelService
{
    private readonly ITravelRepository TravelRepository;

    public TravelService(ITravelRepository travelRepository)
    {
        this.TravelRepository = travelRepository;
    }

    public Travel? FindLastTravelByVesselId(Guid vesselId)
    {
        return this.TravelRepository.FindLastTravelByVesselId(vesselId);
    }

    public void InitDataFromTravelInVesselForHomeView(VesselForHomeView vesselView)
    {
        if (vesselView == null)
        {
            throw new Exception();
        }

        Travel? lastTravel = this.FindLastTravelByVesselId(Guid.Parse(vesselView.Id));

        vesselView.TotalDistance = lastTravel == null ? 0 : this.CalculateTotalDistanceInTravel(lastTravel);
        vesselView.AverageSpeed = lastTravel == null ? 0 : this.CalculateAverageSpeed(lastTravel, vesselView.TotalDistance);
    }

    /// <summary>
    /// Calculate the total distance in the given travel using the position list
    /// It assumes that the X coordinate and Y coordinate are in kilometer.
    /// It assumes that the vessel have gone straight forward between the two positions
    ///     It uses pythagore theorem to calculate the hypothenuse line 
    /// </summary>
    /// <param name="travel">the travel to calculate the distance</param>
    /// <returns>the total distance in the travel</returns>
    private double CalculateTotalDistanceInTravel(Travel travel)
    {
        int currentIndex = 0;
        double totalDistance = 0;
            
        while(travel.Positions?.Count > currentIndex + 1)
        {
            double xDistance = Math.Abs(travel.Positions[currentIndex].X - travel.Positions[currentIndex + 1].X);
            double yDistance = Math.Abs(travel.Positions[currentIndex].Y - travel.Positions[currentIndex + 1].Y);

            // Pythagore theorem to calculate the distance of the 2 points in euclidian map
            totalDistance += Math.Sqrt(Math.Pow(xDistance, 2) + Math.Pow(yDistance, 2));
            currentIndex++;
        }

        return Math.Round(totalDistance, 2);
    }

    /// <summary>
    /// Calculate the average speed of a travel, given the total distance
    /// It take the total distance and calculate the average speed assuming that the vessel
    /// are moving since the first position to the last position
    /// </summary>
    /// <param name="travel">the travel to calculate the average speed</param>
    /// <param name="totalDistance">total distance in the given travel</param>
    /// <returns>average speed</returns>
    private double CalculateAverageSpeed(Travel travel, double totalDistance)
    {
        List<Position> positions = travel.Positions
            .OrderBy(position => position.Timestamp)
            .ToList();

        // Get the first and the last position
        Position begin = positions.First();
        Position last = positions.Last();

        // Get the time spent on then travel
        double spentTime = last.Timestamp.Subtract(begin.Timestamp).TotalHours;
        
        // Calculate the kilometers per hours speed
        return Math.Round((totalDistance / spentTime));
    }
}