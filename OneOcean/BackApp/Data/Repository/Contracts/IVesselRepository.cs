﻿using OneOcean.BackApp.Data.Domain;

namespace OneOcean.BackApp.Data.Repository.Contracts;

public interface IVesselRepository
{
    /// <summary>
    /// Find the vessel related to the given identifier
    /// </summary>
    /// <param name="vesselId">Id of the searched vessel</param>
    /// <returns>the vessel if exists. Null otherwise</returns>
    public Vessel? FindVesselById(Guid vesselId);
}