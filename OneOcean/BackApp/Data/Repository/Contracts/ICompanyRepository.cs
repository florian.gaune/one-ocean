﻿using OneOcean.BackApp.Data.Domain;

namespace OneOcean.BackApp.Data.Repository.Contracts;

public interface ICompanyRepository
{
    /// <summary>
    /// Find the company related to the given id
    /// </summary>
    /// <param name="id">Id of the searched company</param>
    /// <returns>The existing company, or null if it does not exists</returns>
    Company? FindCompanyById(Guid id);
}