﻿using OneOcean.BackApp.Data.Domain;

namespace OneOcean.BackApp.Data.Repository.Contracts;

public interface ITravelRepository
{
    /// <summary>
    /// Find the last travel done by the given vessel
    /// Is considered the last, the travel having the later endDate
    /// </summary>
    /// <param name="vesselId">ID of the vessel related to the searched travel</param>
    /// <returns>the last travel if exists. null otherwise</returns>
    public Travel? FindLastTravelByVesselId(Guid vesselId);
}