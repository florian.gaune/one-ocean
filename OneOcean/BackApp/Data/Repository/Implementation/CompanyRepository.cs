﻿using OneOcean.BackApp.Data.Domain;
using OneOcean.BackApp.Data.Repository.Contracts;

namespace OneOcean.BackApp.Data.Repository.Implementation;

public class CompanyRepository : ICompanyRepository
{
    public Company? FindCompanyById(Guid id)
    {
        return DataMock
            .Companies
            .Find(company => company.Id == id);
    }
}