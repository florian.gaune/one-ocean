﻿using System.Text.Json;
using OneOcean.BackApp.Data.Domain;
using File = System.IO.File;

namespace OneOcean.BackApp.Data.Repository.Implementation;

public class DataMock
{
    public static List<Company> Companies;
    public static List<Vessel> Vessels;
    public static List<Travel> Travels;

    static DataMock()
    {
        List<Company>? deserializedData = JsonSerializer.Deserialize<List<Company>>(ReadMockFile());
        if (deserializedData != null)
        {
            Companies = deserializedData;
            InitVessels();
            InitTravels();
        }
    }

    private static string ReadMockFile()
    {
        return File.ReadAllText("./BackApp/Data/Mock/mock.json");
    }

    private static void InitVessels()
    {
        if (Companies?.Count > 0)
        {
            Vessels = new List<Vessel>();
            foreach (Company company in Companies)
            {
                foreach (Vessel vessel in company.Vessels)
                {
                    vessel.Company = company;
                    Vessels.Add(vessel);
                }
            }
        }
    }

    private static void InitTravels()
    {
        if (Vessels?.Count > 0)
        {
            Travels = new List<Travel>();
            foreach (Vessel vessel in Vessels)
            {
                foreach (Travel travel in vessel.Travels)
                {
                    travel.Vessel = vessel;
                    Travels.Add(travel);
                }
            }
        }
    }
}