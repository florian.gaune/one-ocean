﻿using OneOcean.BackApp.Data.Domain;
using OneOcean.BackApp.Data.Repository.Contracts;

namespace OneOcean.BackApp.Data.Repository.Implementation;

public class VesselRepository : IVesselRepository
{
    public Vessel? FindVesselById(Guid vesselId)
    {
        return DataMock
            .Vessels
            .Find(vessel => vessel.Id == vesselId);
    }
}