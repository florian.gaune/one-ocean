﻿using OneOcean.BackApp.Data.Domain;
using OneOcean.BackApp.Data.Repository.Contracts;

namespace OneOcean.BackApp.Data.Repository.Implementation;

public class TravelRepository : ITravelRepository
{
    public Travel? FindLastTravelByVesselId(Guid vesselId)
    {
        return DataMock.Vessels
            .Find(vessel => vessel.Id == vesselId)
            ?.Travels
            .OrderBy(travel => travel.EndDate)
            .First();
    }
}