﻿namespace OneOcean.BackApp.Data.Domain;

/// <summary>
/// Represent a position of a vessel in the map, in a given time
/// </summary>
public class Position
{
    public int X { get; set; }
    public int Y { get; set; }
    public DateTime Timestamp { get; set; }
}