﻿namespace OneOcean.BackApp.Data.Domain;

/// <summary>
/// Represent a company => A user of the application
/// Each Company has vessels
/// </summary>
public class Company
{
    public Guid Id { get; set; }
    public String Name { get; set; }
    public List<Vessel> Vessels { get; set; }
}