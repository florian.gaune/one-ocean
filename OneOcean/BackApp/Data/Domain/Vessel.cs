﻿namespace OneOcean.BackApp.Data.Domain;

/// <summary>
/// Represent a vessel
/// Each vessel has a list of done or in progress travels
/// </summary>
public class Vessel
{
    public Guid Id { get; set; }
    public Company Company { get; set; }
    public string Name { get; set; }
    public List<Travel> Travels { get; set; }
}