﻿namespace OneOcean.BackApp.Data.Domain;

/// <summary>
/// Represent a vessel travel
/// A travel has a begin date and an end date.
/// Between those two dates, travels have a list of positions for the vessel
/// </summary>
public class Travel
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public Vessel Vessel { get; set; }
    public List<Position> Positions { get; set; }
    public DateTime BeginDate { get; set; }
    public DateTime EndDate { get; set; }
}