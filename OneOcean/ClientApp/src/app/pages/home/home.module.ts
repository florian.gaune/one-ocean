﻿import {NgModule} from "@angular/core";
import {HomeComponent} from "./home.component";
import {HomeRoutingModule} from "./home-routing.module";
import {VesselOverviewComponent} from "./VesselOverview/vessel-overview.component";
import {FormsModule} from "@angular/forms";
import {NgForOf} from "@angular/common";

@NgModule({
  declarations: [HomeComponent, VesselOverviewComponent],
  imports: [HomeRoutingModule, NgForOf]
})
export class HomeModule {};
