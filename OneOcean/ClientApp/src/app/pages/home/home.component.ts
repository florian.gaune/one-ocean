﻿import {Component, OnInit} from "@angular/core";
import {ServiceUsingComponent} from "../../shared/component/serviceUsingComponent";
import {CommonDataSharingService} from "../../shared/Services/Common-data-sharing.service";
import {CompanyBackendService} from "../../shared/Services/Backend/company.backend.service";
import {Company} from "../../shared/Models/company.model";

@Component({
  selector: 'ocean-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent extends ServiceUsingComponent {

  connectedCompany: Company = new Company();

  constructor(private commonDataSharingService: CommonDataSharingService,
              private companyBackendService: CompanyBackendService) {
    super();
  }

  ngOnInit() {
    super.ngOnInit();

    this.subscriptions.add(
      this.companyBackendService
        .findById(this.commonDataSharingService.connectedCompany.id)
        .subscribe(company => {
          this.connectedCompany = company;

          // Update connected company to get all informations
          this.commonDataSharingService.connectedCompany = company;
        })
    );
  }
}
