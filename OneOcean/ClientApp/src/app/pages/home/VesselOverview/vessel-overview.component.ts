﻿import {ServiceUsingComponent} from "../../../shared/component/serviceUsingComponent";
import {Component, Input} from "@angular/core";
import {VesselForOverviewBackendService} from "../../../shared/Services/Backend/vessel.backend.service";
import {VesselForOverview} from "../../../shared/Models/vessel.model";

@Component({
  selector: 'oocean-vessel-overview',
  templateUrl: 'vessel-overview.component.html',
  styleUrls: ['vessel-overview.component.scss']
})
export class VesselOverviewComponent extends ServiceUsingComponent {

  @Input()
  vesselId: string = "";

  vessel: VesselForOverview = new VesselForOverview();

  constructor(private vesselForOverviewBackendService: VesselForOverviewBackendService) {
    super();
  }

  ngOnInit() {
    super.ngOnInit();
    this.subscriptions.add(
      this.vesselForOverviewBackendService
        .findById(this.vesselId)
        .subscribe(vessel => this.vessel = vessel)
    );
  }
}
