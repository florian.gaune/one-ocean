﻿import {Component} from "@angular/core";
import {ServiceUsingComponent} from "../../shared/component/serviceUsingComponent";
import {CompanyBackendService} from "../../shared/Services/Backend/company.backend.service";
import {FontAwesomeIconsService} from "../../shared/fontAwesomeIcons";

@Component({
  selector: 'oocean-upper-banner',
  templateUrl: 'upper-banner.component.html',
  styleUrls: ['upper-banner.component.scss']
})
export class UpperBannerComponent extends ServiceUsingComponent {

  public name: string = '';

  constructor(private companyBackendService: CompanyBackendService,
              public fontAwesomeIconService: FontAwesomeIconsService) {
    super();
  }

  ngOnInit() {
    super.ngOnInit();
    this.subscriptions.add(
      this.companyBackendService
      .findById('cb5d87f5-1464-4556-8165-1c5dd7a17863')
      .subscribe(company => this.name = company.name)
    );
  }
}
