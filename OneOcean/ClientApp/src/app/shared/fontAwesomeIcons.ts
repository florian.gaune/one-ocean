﻿import {Injectable} from "@angular/core";
import { faCircleUser } from '@fortawesome/free-solid-svg-icons';
import {IconDefinition} from "@fortawesome/free-regular-svg-icons";

@Injectable({
  providedIn: "root"
})
export class FontAwesomeIconsService {

  public circleUser: IconDefinition = faCircleUser;

  constructor() {}
}
