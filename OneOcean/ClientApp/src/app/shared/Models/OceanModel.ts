﻿/**
 * This class allows to generify front's model.
 * The only needed attribute is the identifier
 * This class will allows to use OSS services
 * T is the type of the final model
 *
 * Warning ! It is mandatory to implement toJSON method with at least return this.
 *          If you didn't do that, the JSON object sent to the server will be empty.
 * @see OSSBackendService
 */
export abstract class OceanModel<T> {
  /**
   * Init the this object from a given json object
   * @param data in json format
   * @return the final model instance initiated with the given json object
   */
  public abstract initFromJSON(data: any): T;

  /**
   * Return an object json representing this instance
   * This method is automatically used to send model on network.
   * At least, return this, otherwise, the front will send empty object
   */
  public abstract toJSON(): any;

  constructor(public id?: any) {}
}

/* ==============================
            Example of use
   ==============================


   export class Ordinateur extends OceanModel<Ordinateur> {

        constructor(id: any, ...) {
            super(id);
        }

        public toJSON(): any {
            return this;
        }


 */
