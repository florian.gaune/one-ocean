﻿import {OceanModel} from "./OceanModel";

export class VesselForOverview extends OceanModel<VesselForOverview> {

  constructor(
    public id: string = "",
    public name: string = "",
    public averageSpeed: number = 0,
    public totalDistance: number = 0
  ) {
    super();
  }

  initFromJSON(data: any): VesselForOverview {
    return Object.assign(this, data);
  }

  toJSON(): any {
    return this;
  }
}
