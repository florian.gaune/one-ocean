﻿import {OceanModel} from "./OceanModel";

export class Company extends OceanModel<Company> {

  constructor(
    public id: string = '',
    public name: string = '',
    public vesselIdArray: string[] = []
  ) {
    super();
  }

  initFromJSON(data: any): Company {
    return Object.assign(this, data);
  }

  toJSON(): any {
    return this;
  }

}
