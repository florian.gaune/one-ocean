﻿import {Component, OnDestroy, OnInit} from "@angular/core";
import {Subscription} from "rxjs";

/**
 * This component uses services.
 * On Destroying it, all subscriptions in the related variable are destroyed.
 */
@Component({
  template: ''
})
export abstract class ServiceUsingComponent implements OnDestroy, OnInit {
  protected subscriptions: Subscription;

  constructor() {
    this.subscriptions = new Subscription();
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  ngOnInit(): void {
    this.subscriptions = new Subscription();
  }
}
