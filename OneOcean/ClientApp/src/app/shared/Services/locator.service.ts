﻿import { Injector } from '@angular/core';

/**
 * The LocatorService is used to inject dependencies programmatically in TS files.
 * To use it, the application has to set this injector in app.module (the main module of the application)
 * example : in main.module.ts :
 *  export class mainModule {
 *       constructor(private injector: Injector) {
 *           LocatorService.injector = injector;
 *       }
 *  }
 */
export class LocatorService {
  static injector: Injector;
}
