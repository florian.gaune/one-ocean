﻿import {Injectable} from "@angular/core";
import {Company} from "../Models/company.model";

@Injectable({
  providedIn: 'root',
})
export class CommonDataSharingService {

  public connectedCompany: Company = new Company('cb5d87f5-1464-4556-8165-1c5dd7a17863');
}
