﻿import {BackendService} from "./backend.service";
import {Company} from "../../Models/company.model";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root',
})
export class CompanyBackendService extends BackendService<Company, CompanyBackendService> {

  protected getInstance(): CompanyBackendService {
    return this;
  }

  protected getNewObjectInstance(): Company {
    return new Company();
  }

  protected getSpecificURL(innerParam: string | undefined): string {
    return "company";
  }

}
