﻿import {BackendService} from "./backend.service";
import {VesselForOverview} from "../../Models/vessel.model";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root',
})
export class VesselForOverviewBackendService extends BackendService<VesselForOverview, VesselForOverviewBackendService> {
  protected getInstance(): VesselForOverviewBackendService {
    return this;
  }

  protected getNewObjectInstance(): VesselForOverview {
    return new VesselForOverview();
  }

  protected getSpecificURL(innerParam: string | undefined): string {
    return "vessel";
  }

}
