﻿import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SERVER_API_URL } from './../../../app.constants';
import { map } from 'rxjs/operators';
import {OceanModel} from "../../Models/OceanModel";
import {LocatorService} from "../locator.service";

/**
 * This class is a generic backend service.
 * It allows basic CRUD Operations (findById, findAll, create, update, delete)
 */
export abstract class BackendService<T extends OceanModel<T>, B extends BackendService<T, B>> {
  // client HTTP to make request to the back-end
  protected http: HttpClient;

  // Used to have inner params in URL. Example : we have books in library object. If we want books of library A, the REST URL is api/library/A/books
  private innerParam: any = null;

  /**
   * This method has to be implemented in subclass and has to return a new object of type handled by the backend service subclass.
   * It used for create return data on methods
   * @protected
   */
  protected abstract getNewObjectInstance(): T;

  constructor() {
    this.http = LocatorService.injector.get<HttpClient>(HttpClient);
  }

  protected abstract getInstance(): B;

  public withInnerParam(innerParam: any): B {
    this.innerParam = innerParam;
    return this.getInstance();
  }

  // region CRUD methods

  /**
   * find the entity from its id
   * @param searchedId the id to search
   * @param spinnerMessage
   */
  public findById(searchedId: any): Observable<T> {
    const url = `${this.getURL(this.popInnerParam())}/${searchedId}`;
    return this.http
      .get<T>(url, { observe: 'response' })
      .pipe(map((res: HttpResponse<T>) => this.createUniqueDataFromJSON(res.body)));
  }

  /**
   * find all entites
   * @param spinnerMessage
   */
  public findAll(): Observable<T[]> {
    const url = this.getURL(this.popInnerParam());
    return this.http.get(url, { observe: 'response' }).pipe(map((res: HttpResponse<any>) => this.createListDataFromJSON(res.body)));
  }

  /**
   * update the given data and get it
   * @param dataToUpdate the data to update
   * @param spinnerMessage
   */
  public update(dataToUpdate: T): Observable<T> {
    const url = this.getURL(this.popInnerParam());
    return this.http
      .put<T>(url, dataToUpdate.toJSON(), { observe: 'response' })
      .pipe(map((res: HttpResponse<T>) => this.createUniqueDataFromJSON(res.body)));
  }

  /**
   * Create the given data and get it
   * @param dataToCreate the data to create
   * @param spinnerMessage
   */
  public create(dataToCreate: T): Observable<T> {
    const url = this.getURL(this.popInnerParam()) + '/unique';
    return this.http
      .put<T>(url, dataToCreate.toJSON(), {
        observe: 'response',
      })
      .pipe(map((res: HttpResponse<T>) => this.createUniqueDataFromJSON(res.body)));
  }

  /**
   * Delete an entity identified by the idToDelete
   * @param spinnerMessage
   * @param idToDelete
   */
  public delete(idToDelete: any): Observable<any> {
    const url = `${this.getURL(this.popInnerParam())}/${idToDelete}`;
    return this.http.delete(url, { observe: 'response' }).pipe(map(res => res.body));
  }

  // endregion

  // region Core methods

  /**
   * Get the url to call in each request
   */
  protected getURL(innerParam?: string): string {
    return SERVER_API_URL + '/' + this.getSpecificURL(innerParam);
  }

  /**
   * Get the specific url after '/api'
   */
  protected abstract getSpecificURL(innerParam?: string): string;

  protected popInnerParam(): string {
    const innerParam = this.innerParam;
    this.innerParam = null;
    return innerParam;
  }

  /**
   * Create a new Data of T type
   * @param data
   */
  protected createUniqueDataFromJSON(data: any): T {
    return this.getNewObjectInstance().initFromJSON(data);
  }

  /**
   * Create a new data array of T type
   * @param data
   */
  protected createListDataFromJSON(data: any[]): T[] {
    return data.map(element => this.createUniqueDataFromJSON(element));
  }

  // endregion
}
