import { Component } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./../style/global.scss']
})
export class AppComponent {
  title = 'app';
}
